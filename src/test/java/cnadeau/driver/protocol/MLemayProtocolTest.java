package cnadeau.driver.protocol;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MLemayProtocolTest
{
	private MLemayProtocol protocol;

	@Before
	public void before()
	{
		protocol = new MLemayProtocol();
	}

	@Test
	public void onA1()
	{
		// A1ON 0x20 0x60 0x9f 0x00 0xff
		byte[] result = protocol.getBytesForX10Command('+', 'a', 1);
		assertEquals((byte) 0x60, result[1]);
		assertEquals((byte) 0x9f, result[2]);
		assertEquals((byte) 0x00, result[3]);
		assertEquals((byte) 0xff, result[4]);

	}

	@Test
	public void offA1()
	{
		// A1OFF 0x20 0x60 0x9f 0x20 0xdf
		byte[] result = protocol.getBytesForX10Command('-', 'a', 1);
		assertEquals((byte) 0x60, result[1]);
		assertEquals((byte) 0x9f, result[2]);
		assertEquals((byte) 0x20, result[3]);
		assertEquals((byte) 0xdf, result[4]);
	}

	@Test
	public void onB2()
	{
		// B2ON 0x20 0x70 0x8f 0x10 0xef
		byte[] result = protocol.getBytesForX10Command('+', 'b', 2);
		assertEquals((byte) 0x70, result[1]);
		assertEquals((byte) 0x8f, result[2]);
		assertEquals((byte) 0x10, result[3]);
		assertEquals((byte) 0xef, result[4]);
	}

	@Test
	public void offB2()
	{
		// B2OFF 0x20 0x70 0x8f 0x30 0xcf
		byte[] result = protocol.getBytesForX10Command('-', 'b', 2);
		assertEquals((byte) 0x70, result[1]);
		assertEquals((byte) 0x8f, result[2]);
		assertEquals((byte) 0x30, result[3]);
		assertEquals((byte) 0xcf, result[4]);
	}
}
