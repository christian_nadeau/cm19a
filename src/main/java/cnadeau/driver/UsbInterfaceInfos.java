package cnadeau.driver;

import java.util.List;

import javax.usb.UsbConst;
import javax.usb.UsbEndpoint;
import javax.usb.UsbInterface;

import cnadeau.driver.exceptions.CM19AException;

public class UsbInterfaceInfos
{
	private UsbEndpoint inEndpoint;
	private UsbEndpoint outEndpoint;
	private final UsbInterface usbInterface;

	public UsbInterfaceInfos(UsbInterface usbInterface)
	{
		this.usbInterface = usbInterface;
		List usbEndpoints = usbInterface.getUsbEndpoints();

		for (int i = 0; i < usbEndpoints.size(); i++)
		{
			UsbEndpoint endpoint = (UsbEndpoint) usbEndpoints.get(i);

			if (UsbConst.ENDPOINT_DIRECTION_IN == endpoint.getDirection())
			{
				inEndpoint = endpoint;
			}
			else if (UsbConst.ENDPOINT_DIRECTION_OUT == endpoint.getDirection())
			{
				outEndpoint = endpoint;
			}
		}

		if (getInEndpoint() == null || getOutEndpoint() == null)
		{
			throw new CM19AException("Couldn't find both endpoints");
		}
	}

	public UsbEndpoint getInEndpoint()
	{
		return inEndpoint;
	}

	public UsbEndpoint getOutEndpoint()
	{
		return outEndpoint;
	}

	public UsbInterface getUsbInterface()
	{
		return usbInterface;
	}
}
