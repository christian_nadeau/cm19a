package cnadeau.driver.stub;

import java.util.ArrayList;
import java.util.List;

import javax.usb.UsbConst;
import javax.usb.UsbControlIrp;
import javax.usb.UsbDisconnectedException;
import javax.usb.UsbEndpoint;
import javax.usb.UsbException;
import javax.usb.UsbIrp;
import javax.usb.UsbNotActiveException;
import javax.usb.UsbNotClaimedException;
import javax.usb.UsbNotOpenException;
import javax.usb.UsbPipe;
import javax.usb.event.UsbPipeDataEvent;
import javax.usb.event.UsbPipeListener;

public class StubUsbPipe implements UsbPipe
{
	private final UsbEndpoint usbEndpoint;
	private final List<UsbPipeListener> listeners;

	public StubUsbPipe(UsbEndpoint usbEndpoint)
	{
		listeners = new ArrayList<UsbPipeListener>();
		this.usbEndpoint = usbEndpoint;
	}

	@Override
	public void open() throws UsbException, UsbNotActiveException, UsbNotClaimedException, UsbDisconnectedException
	{}

	@Override
	public void close() throws UsbException, UsbNotActiveException, UsbNotOpenException, UsbDisconnectedException
	{}

	@Override
	public boolean isActive()
	{
		return false;
	}

	@Override
	public boolean isOpen()
	{
		return false;
	}

	@Override
	public UsbEndpoint getUsbEndpoint()
	{
		return usbEndpoint;
	}

	@Override
	public int syncSubmit(byte[] data) throws UsbException, UsbNotActiveException, UsbNotOpenException, IllegalArgumentException, UsbDisconnectedException
	{
		// Only stub output since input need to read incoming bytes and put them in the received buffer
		if (getUsbEndpoint().getDirection() == UsbConst.ENDPOINT_DIRECTION_OUT)
		{
			for (UsbPipeListener listener : listeners)
			{
				listener.dataEventOccurred(new UsbPipeDataEvent(this, data, data.length));
			}
		}
		else
		{
			try
			{
				Thread.sleep(1000);
			}
			catch (InterruptedException e)
			{}
		}
		return 0;
	}

	@Override
	public UsbIrp asyncSubmit(byte[] data) throws UsbException, UsbNotActiveException, UsbNotOpenException, IllegalArgumentException, UsbDisconnectedException
	{
		return null;
	}

	@Override
	public void syncSubmit(UsbIrp irp) throws UsbException, UsbNotActiveException, UsbNotOpenException, IllegalArgumentException, UsbDisconnectedException
	{}

	@Override
	public void asyncSubmit(UsbIrp irp) throws UsbException, UsbNotActiveException, UsbNotOpenException, IllegalArgumentException, UsbDisconnectedException
	{}

	@Override
	public void syncSubmit(List list) throws UsbException, UsbNotActiveException, UsbNotOpenException, IllegalArgumentException, UsbDisconnectedException
	{}

	@Override
	public void asyncSubmit(List list) throws UsbException, UsbNotActiveException, UsbNotOpenException, IllegalArgumentException, UsbDisconnectedException
	{}

	@Override
	public void abortAllSubmissions() throws UsbNotActiveException, UsbNotOpenException, UsbDisconnectedException
	{}

	@Override
	public UsbIrp createUsbIrp()
	{
		return null;
	}

	@Override
	public UsbControlIrp createUsbControlIrp(byte bmRequestType, byte bRequest, short wValue, short wIndex)
	{
		return null;
	}

	@Override
	public void addUsbPipeListener(UsbPipeListener listener)
	{
		listeners.add(listener);
	}

	@Override
	public void removeUsbPipeListener(UsbPipeListener listener)
	{
		listeners.remove(listener);
	}

}
