package cnadeau.driver.stub;

import java.io.UnsupportedEncodingException;

import javax.usb.UsbException;

import cnadeau.driver.CM19AUsbDeviceLookup;
import cnadeau.driver.UsbInterfaceInfos;
import cnadeau.driver.exceptions.CM19AException;

public class StubCM19AUsbDeviceLookup implements CM19AUsbDeviceLookup
{

	@Override
	public UsbInterfaceInfos findCM19ADevice() throws CM19AException, UsbException, UnsupportedEncodingException
	{
		return new UsbInterfaceInfos(new StubUsbInterface());
	}

}
