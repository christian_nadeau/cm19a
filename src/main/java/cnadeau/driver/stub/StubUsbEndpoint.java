package cnadeau.driver.stub;

import javax.usb.UsbEndpoint;
import javax.usb.UsbEndpointDescriptor;
import javax.usb.UsbInterface;
import javax.usb.UsbPipe;

public class StubUsbEndpoint implements UsbEndpoint
{

	private final UsbPipe usbPipe;
	private final UsbInterface usbInterface;
	private final byte direction;

	public StubUsbEndpoint(UsbInterface usbInterface, byte direction)
	{
		this.usbInterface = usbInterface;
		this.direction = direction;
		this.usbPipe = new StubUsbPipe(this);
	}

	@Override
	public UsbInterface getUsbInterface()
	{
		return usbInterface;
	}

	@Override
	public UsbEndpointDescriptor getUsbEndpointDescriptor()
	{
		return null;
	}

	@Override
	public byte getDirection()
	{
		return direction;
	}

	@Override
	public byte getType()
	{
		return 0;
	}

	@Override
	public UsbPipe getUsbPipe()
	{
		return usbPipe;
	}
}
