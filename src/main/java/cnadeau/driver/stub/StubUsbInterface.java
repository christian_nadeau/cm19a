package cnadeau.driver.stub;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;

import javax.usb.UsbClaimException;
import javax.usb.UsbConfiguration;
import javax.usb.UsbConst;
import javax.usb.UsbDisconnectedException;
import javax.usb.UsbEndpoint;
import javax.usb.UsbException;
import javax.usb.UsbInterface;
import javax.usb.UsbInterfaceDescriptor;
import javax.usb.UsbInterfacePolicy;
import javax.usb.UsbNotActiveException;

public class StubUsbInterface implements UsbInterface
{
	private final UsbEndpoint inUsbEndpoint;
	private final StubUsbEndpoint outUsbEndpoint;

	public StubUsbInterface()
	{
		this.inUsbEndpoint = new StubUsbEndpoint(this, UsbConst.ENDPOINT_DIRECTION_IN);
		this.outUsbEndpoint = new StubUsbEndpoint(this, UsbConst.ENDPOINT_DIRECTION_OUT);
	}

	@Override
	public void claim() throws UsbClaimException, UsbException, UsbNotActiveException, UsbDisconnectedException
	{
	}

	@Override
	public void claim(UsbInterfacePolicy policy) throws UsbClaimException, UsbException, UsbNotActiveException, UsbDisconnectedException
	{
	}

	@Override
	public void release() throws UsbClaimException, UsbException, UsbNotActiveException, UsbDisconnectedException
	{
	}

	@Override
	public boolean isClaimed()
	{
		return false;
	}

	@Override
	public boolean isActive()
	{
		return false;
	}

	@Override
	public int getNumSettings()
	{
		return 0;
	}

	@Override
	public byte getActiveSettingNumber() throws UsbNotActiveException
	{
		return 0;
	}

	@Override
	public UsbInterface getActiveSetting() throws UsbNotActiveException
	{
		return null;
	}

	@Override
	public UsbInterface getSetting(byte number)
	{
		return null;
	}

	@Override
	public boolean containsSetting(byte number)
	{
		return false;
	}

	@Override
	public List getSettings()
	{
		return null;
	}

	@Override
	public List getUsbEndpoints()
	{
		return Arrays.asList(inUsbEndpoint, outUsbEndpoint);
	}

	@Override
	public UsbEndpoint getUsbEndpoint(byte address)
	{
		return null;
	}

	@Override
	public boolean containsUsbEndpoint(byte address)
	{
		return false;
	}

	@Override
	public UsbConfiguration getUsbConfiguration()
	{
		return null;
	}

	@Override
	public UsbInterfaceDescriptor getUsbInterfaceDescriptor()
	{
		return null;
	}

	@Override
	public String getInterfaceString() throws UsbException, UnsupportedEncodingException, UsbDisconnectedException
	{
		return null;
	}
}
