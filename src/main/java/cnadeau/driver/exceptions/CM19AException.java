package cnadeau.driver.exceptions;

public class CM19AException extends RuntimeException
{
	public CM19AException()
	{
		super();
	}

	public CM19AException(String message)
	{
		super(message);
	}

	public CM19AException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
