package cnadeau.driver;

public interface CM19AInputReader
{
	int read(byte[] buffer);

	void close();
}
