package cnadeau.driver;

import java.io.File;
import java.io.OutputStream;
import java.io.PrintWriter;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;

import cnadeau.datatransmision.FilePipeReader;
import cnadeau.datatransmision.SocketReader;
import cnadeau.driver.protocol.MLemayProtocol;

public class DriverMainClass
{
	public static void main(String[] commandLineArguments)
	{
		final CommandLineParser cmdLineGnuParser = new GnuParser();
		final Options gnuOptions = constructGnuOptions();

		try
		{
			CommandLine commandLine = cmdLineGnuParser.parse(gnuOptions, commandLineArguments);

			if (commandLineArguments.length < 1 || commandLine.hasOption("help"))
			{
				printHelp(gnuOptions, 80, "CM19A driver", "", 3, 5, true, System.out);
			}
			else
			{
				CM19AInputReader inputReader;
				if (commandLine.hasOption("f"))
				{
					inputReader = new FilePipeReader(new File(commandLine.getOptionValue("f")));
				}
				else if (commandLine.hasOption("sp"))
				{
					inputReader = new SocketReader(Integer.valueOf(commandLine.getOptionValue("sp")));
				}
				else
				{
					inputReader = new SocketReader(10000);
				}

				CM19ADevice cm19a = new CM19ADevice(new CM19AUsbDeviceLookupImpl(), inputReader, new MLemayProtocol());
				cm19a.start();
			}
		}
		catch (Exception exception)
		{
			System.err.println("Encountered exception while starting driver:\n" + exception.getMessage());
		}
	}

	public static Options constructGnuOptions()
	{
		final Options gnuOptions = new Options();
		gnuOptions.addOption("h", "help", false, "Help")
				.addOption("f", "pipefile", true, "Pipe file in which commands will be read from the driver (will be deleted and recreated)")
				.addOption("sp", "socketPort", true, "Port on which a ServerSocket will be listening for commands");
		return gnuOptions;
	}

	/**
	 * Write "help" to the provided OutputStream.
	 */
	public static void printHelp(final Options options, final int printedRowWidth, final String header, final String footer, final int spacesBeforeOption,
			final int spacesBeforeOptionDescription, final boolean displayUsage, final OutputStream out)
	{
		final String commandLineSyntax = "java -jar x10.cm19a.x.x.x.jar";
		final PrintWriter writer = new PrintWriter(out);
		final HelpFormatter helpFormatter = new HelpFormatter();
		helpFormatter.printHelp(writer, printedRowWidth, commandLineSyntax, header, options, spacesBeforeOption, spacesBeforeOptionDescription, footer,
				displayUsage);
		writer.close();
	}
}
