package cnadeau.driver.threads;

import javax.usb.UsbPipe;

public class ReceiverThread extends DataThread
{
	public ReceiverThread(UsbPipe pipe)
	{
		super(pipe, new PipeListener("the CM19a device to the host"));
		this.setName("CM19A usb data receiver thread");
	}

	@Override
	protected byte[] getDataToSubmitToPipe()
	{
		return new byte[64];
	}
}