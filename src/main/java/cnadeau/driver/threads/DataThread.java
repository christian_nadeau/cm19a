package cnadeau.driver.threads;

import javax.usb.UsbPipe;
import javax.usb.event.UsbPipeListener;

import org.apache.log4j.Logger;

import cnadeau.enums.AvailableLoggers;

import com.google.common.base.Throwables;

public abstract class DataThread extends Thread
{
	Logger logger = Logger.getLogger(AvailableLoggers.DataThread.toString());

	private UsbPipe pipe = null;
	private final UsbPipeListener pipeListener;
	public boolean running = true;

	public DataThread(UsbPipe pipe, UsbPipeListener pipeListener)
	{
		this.pipeListener = pipeListener;
		this.pipe = pipe;
		this.pipe.addUsbPipeListener(pipeListener);
	}

	protected abstract byte[] getDataToSubmitToPipe();

	@Override
	public void run()
	{
		while (running)
		{
			try
			{
				byte[] buffer = getDataToSubmitToPipe();
				pipe.syncSubmit(buffer);
				// Data received
			}
			catch (Exception e)
			{
				if (running)
				{
					logger.error("Unable to submit data buffer to CM19a : " + e.getMessage());
					Throwables.propagate(e);
				}
			}
		}
	}

	/**
	 * Stop/abort listening for data events.
	 */
	public void quit()
	{
		running = false;
		pipe.removeUsbPipeListener(pipeListener);
		pipe.abortAllSubmissions();
		try
		{
			pipe.close();
		}
		catch (Exception e)
		{
			// Ignore exception
		}
	}
}
