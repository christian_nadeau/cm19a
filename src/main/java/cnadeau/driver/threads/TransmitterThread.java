package cnadeau.driver.threads;

import javax.usb.UsbPipe;

import cnadeau.driver.CM19ADevice;

/**
 * Transmits data from the host to the CM19a. This is the class that actually controls the X10 devices by having the CM19a send out the RF protocol for the
 * users input command.
 * 
 * @author Jan Roberts
 * 
 */
public class TransmitterThread extends DataThread
{
	private final CM19ADevice cm19a;

	public TransmitterThread(CM19ADevice cm19a, UsbPipe pipe)
	{
		super(pipe, new PipeListener("the host to the CM19a device"));
		this.cm19a = cm19a;
		this.setName("CM19A usb data transmitter thread");
	}

	@Override
	protected byte[] getDataToSubmitToPipe()
	{
		byte[] sendThis = null;

		/* Keep processing messages or sleep if there are none */
		synchronized (this.cm19a.getOutQueue())
		{
			if (this.cm19a.getOutQueue().size() == 0)
				try
				{
					this.cm19a.getOutQueue().wait();
				}
				catch (InterruptedException e)
				{
					// Do nothing
				}
		}

		synchronized (this.cm19a.getOutQueue())
		{
			if (this.cm19a.getOutQueue().size() > 0)
			{
				sendThis = this.cm19a.getOutQueue().removeFirst();
			}
		}

		return sendThis;
	}
}