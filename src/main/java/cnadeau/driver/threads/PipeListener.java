package cnadeau.driver.threads;

import javax.usb.UsbException;
import javax.usb.event.UsbPipeDataEvent;
import javax.usb.event.UsbPipeErrorEvent;
import javax.usb.event.UsbPipeListener;
import javax.usb.util.UsbUtil;

import org.apache.log4j.Logger;

import cnadeau.enums.AvailableLoggers;

/**
 * This class implements the UsbPipeListener interface by simply printing out all data that is transferred between the CM19a and the host as hex bytes
 * 
 * @author Jan Roberts
 * 
 */
public class PipeListener implements UsbPipeListener
{
	Logger logger = Logger.getLogger(AvailableLoggers.DataThread.toString());

	private String inOrOutMessage = null;

	public PipeListener(String inOrOutMessage)
	{
		this.inOrOutMessage = inOrOutMessage;
	}

	@Override
	public void dataEventOccurred(UsbPipeDataEvent event)
	{
		byte[] message = event.getData();
		int length = event.getActualLength();
		if (message != null)
		{
			/* Print out the message that the CM19a sent */
			logger.info(length + " bytes of data traveled from " + inOrOutMessage);

			if (logger.isDebugEnabled())
			{
				StringBuilder builder = new StringBuilder(": ");
				for (int i = 0; i < length; i++)
				{
					builder.append(" 0x" + UsbUtil.toHexString(message[i]));
				}
				logger.debug(builder.toString());
			}
		}

	}

	@Override
	public void errorEventOccurred(UsbPipeErrorEvent event)
	{
		UsbException exception = event.getUsbException();

		logger.error("Error traveled from " + inOrOutMessage + ": " + exception.getMessage());
	}
}