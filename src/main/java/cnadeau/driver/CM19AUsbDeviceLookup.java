package cnadeau.driver;

import java.io.UnsupportedEncodingException;

import javax.usb.UsbException;

import cnadeau.driver.exceptions.CM19AException;

public interface CM19AUsbDeviceLookup
{
	UsbInterfaceInfos findCM19ADevice() throws CM19AException, UsbException, UnsupportedEncodingException;
}
