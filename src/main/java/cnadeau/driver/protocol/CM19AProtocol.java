package cnadeau.driver.protocol;

public interface CM19AProtocol
{
	public static byte[] CLOSE_DRIVER_BYTES = new byte[] {};

	byte[] convertCommandToRfTransmitterString(String command);

	String getName();
}
