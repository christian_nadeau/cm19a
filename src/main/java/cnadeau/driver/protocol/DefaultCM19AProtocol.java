package cnadeau.driver.protocol;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import org.apache.log4j.Logger;

import cnadeau.driver.exceptions.CM19AException;
import cnadeau.enums.AvailableLoggers;

import com.google.common.base.Throwables;

public class DefaultCM19AProtocol extends Properties implements CM19AProtocol
{
	Logger logger = Logger.getLogger(AvailableLoggers.Protocol.toString());
	public final String PROTOCOL_FILE = "cm19a.protocol";

	public DefaultCM19AProtocol()
	{
		InputStream file = ClassLoader.getSystemResourceAsStream(PROTOCOL_FILE);

		if (file == null)
		{
			throw new CM19AException("Protocol file " + PROTOCOL_FILE + " not found");
		}

		BufferedReader in = null;
		try
		{
			in = new BufferedReader(new InputStreamReader(file));
			String line;
			while ((line = in.readLine()) != null)
			{
				String[] tokens = line.split("\\s+", 2);
				setProperty(tokens[0], tokens[1]);
			}
		}
		catch (IOException e)
		{
			Throwables.propagate(e);
		}
		finally
		{
			if (in == null)
			{
				try
				{
					in.close();
				}
				catch (IOException e)
				{}
			}
		}
	}

	@Override
	public byte[] convertCommandToRfTransmitterString(String command)
	{
		if(command != null && command.equals("exit"))
		{
			return CLOSE_DRIVER_BYTES;
		}
		
		String rfTransmittalAsString = getProperty(command);

		if (rfTransmittalAsString == null)
		{
			logger.warn("Warning: " + command + " not part of the recognized protocol " + getName() + ", ignoring");
			return null;
		}

		String[] s = rfTransmittalAsString.split("\\s+");

		byte[] rfTransmittal = new byte[s.length];
		for (int i = 0; i < s.length; i++)
		{
			rfTransmittal[i] = (byte) Integer.decode(s[i]).intValue();
		}

		return rfTransmittal;
	}

	@Override
	public String getName()
	{
		return getClass().getSimpleName();
	}

}
