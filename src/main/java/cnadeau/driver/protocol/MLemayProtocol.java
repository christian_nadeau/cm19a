package cnadeau.driver.protocol;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import cnadeau.enums.AvailableLoggers;

public class MLemayProtocol implements CM19AProtocol
{
	static Logger logger = Logger.getLogger(AvailableLoggers.Protocol.toString());

	int NORM_CMD_PFX = 0x20;

	// Normal command length
	int NORM_CMD_LEN = 5;
	// Pan'n'Tilt command length
	int CAM_CMD_LEN = 4;

	Map<Character, Integer> CmdCodes = new HashMap<Character, Integer>();
	Map<Character, Integer> HouseCodes = new HashMap<Character, Integer>();
	int[] UnitCodes;

	public MLemayProtocol()
	{
		HouseCodes.put('a', 0x060);
		HouseCodes.put('b', 0x070);
		HouseCodes.put('c', 0x040);
		HouseCodes.put('d', 0x050);
		HouseCodes.put('e', 0x080);
		HouseCodes.put('f', 0x090);
		HouseCodes.put('g', 0x0A0);
		HouseCodes.put('h', 0x0B0);
		HouseCodes.put('i', 0x0E0);
		HouseCodes.put('j', 0x0F0);
		HouseCodes.put('k', 0x0C0);
		HouseCodes.put('l', 0x0D0);
		HouseCodes.put('m', 0x000);
		HouseCodes.put('n', 0x010);
		HouseCodes.put('o', 0x020);
		HouseCodes.put('p', 0x030);

		CmdCodes.put('+', 0x000);
		CmdCodes.put('-', 0x020);
		CmdCodes.put('b', 0x088);
		CmdCodes.put('s', 0x098);

		CmdCodes.put('u', 0x0762);
		CmdCodes.put('d', 0x0863);
		CmdCodes.put('l', 0x0560);
		CmdCodes.put('r', 0x0661);

		UnitCodes = new int[] { 0x0000, 0x0010, 0x0008, 0x0018, 0x0040, 0x0050, 0x0048, 0x0058, 0x0400, 0x0410, 0x0408, 0x0418, 0x0440, 0x0450, 0x0448, 0x0458 };
	}

	@Override
	public byte[] convertCommandToRfTransmitterString(String command)
	{
		if (command != null && command.equals("exit"))
		{
			return CLOSE_DRIVER_BYTES;
		}

		try
		{
			char action = command.charAt(0);
			char houseCode = command.charAt(1);

			int unitCode;
			if (command.length() < 4)
			{
				unitCode = command.charAt(2) - '0';
			}
			else
			{
				unitCode = Integer.parseInt(command.substring(2, 4));
			}

			return getBytesForX10Command(action, houseCode, unitCode);
		}
		catch (NumberFormatException e)
		{
			String safeCommand = (command == null) ? "(empty)" : command;
			logger.error("Unable to handle command '" + safeCommand + "', ignoring it");
			return null;
		}
	}

	byte[] getBytesForX10Command(char command, char house, int unitCode)
	{
		byte[] buffer = new byte[5];
		buffer[0] = (byte) NORM_CMD_PFX;
		buffer[1] = (byte) ((UnitCodes[unitCode - 1] >> 8) | HouseCodes.get(house).intValue());
		buffer[2] = (byte) (0x0FF & ~buffer[1]);
		buffer[3] = (byte) ((UnitCodes[unitCode - 1] & 0x0FF) | CmdCodes.get(command).intValue());
		buffer[4] = (byte) (0x0FF & ~buffer[3]);
		return buffer;
	}

	@Override
	public String getName()
	{
		return getClass().getSimpleName();
	}

}
