package cnadeau.enums;

public enum AvailableLoggers
{
	Protocol, Driver, DataThread
}
