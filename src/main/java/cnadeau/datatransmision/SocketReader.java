package cnadeau.datatransmision;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Semaphore;

import org.apache.log4j.Logger;

import cnadeau.driver.CM19AInputReader;
import cnadeau.enums.AvailableLoggers;

import com.google.common.base.Throwables;

public class SocketReader implements CM19AInputReader
{
	static Logger logger = Logger.getLogger(AvailableLoggers.Driver.toString());
	private ServerSocket serverSocket;
	private final Semaphore availableSemaphore = new Semaphore(1);

	public SocketReader(int port)
	{
		try
		{
			serverSocket = new ServerSocket(port);
		}
		catch (IOException e)
		{
			Throwables.propagate(e);
		}
	}

	@Override
	public int read(byte[] buffer)
	{
		logger.info("Waiting for connection");

		try
		{
			// Release previously aquired semaphore here to ensure a single available/read loop
			availableSemaphore.release();
			availableSemaphore.acquire();
		}
		catch (InterruptedException e)
		{
			Throwables.propagate(e);
		}

		InputStream inputStream = null;
		Socket acceptedConnection = null;
		try
		{
			acceptedConnection = serverSocket.accept();
			inputStream = acceptedConnection.getInputStream();
			acceptedConnection.getOutputStream().write(0x7);
			return inputStream.read(buffer);
		}
		catch (IOException e)
		{
			Throwables.propagate(e);
		}
		finally
		{
			try
			{
				inputStream.close();

			}
			catch (Exception e)
			{}
			try
			{
				acceptedConnection.close();
			}
			catch (Exception e)
			{}
		}

		return 0;
	}

	@Override
	public void close()
	{
		try
		{
			serverSocket.close();
		}
		catch (IOException e)
		{}
	}
}
