package cnadeau.datatransmision;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class SocketTester
{
	private Socket socket;
	private OutputStream outputStream;

	void run()
	{
		try
		{
			socket = new Socket("localhost", 10000);
			System.out.println("Connected to localhost in port 10000");
			outputStream = socket.getOutputStream();
			outputStream.write("-a1".getBytes());
			outputStream.flush();

		}
		catch (UnknownHostException unknownHost)
		{
			System.err.println("You are trying to connect to an unknown host!");
		}
		catch (IOException ioException)
		{
			ioException.printStackTrace();
		}
		finally
		{
			// 4: Closing connection
			try
			{
				outputStream.close();
				socket.close();
			}
			catch (IOException ioException)
			{
				ioException.printStackTrace();
			}
		}
	}

	public static void main(String args[])
	{
		SocketTester client = new SocketTester();
		client.run();
	}
}
