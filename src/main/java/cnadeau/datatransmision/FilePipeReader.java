package cnadeau.datatransmision;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import cnadeau.driver.CM19AInputReader;

import com.google.common.base.Throwables;

public class FilePipeReader implements CM19AInputReader
{
	private BufferedReader bufferedInputStream;
	private final File pipeFile;

	public FilePipeReader(File pipeFile)
	{
		this.pipeFile = pipeFile;
		if (this.pipeFile.exists())
		{
			this.pipeFile.delete();
		}

		try
		{
			Runtime.getRuntime().exec("mkfifo " + pipeFile.getAbsolutePath()).waitFor();
		}
		catch (Exception e)
		{
			Throwables.propagate(e);
		}
	}

	@Override
	public int read(byte[] buffer)
	{
		try
		{
			init();
			while (!bufferedInputStream.ready())
			{
				try
				{
					Thread.sleep(250);
				}
				catch (InterruptedException e)
				{}
			}
			byte[] readLineBytes = bufferedInputStream.readLine().getBytes();
			System.arraycopy(readLineBytes, 0, buffer, 0, readLineBytes.length);
			return readLineBytes.length;
		}
		catch (IOException e)
		{
			Throwables.propagate(e);
		}

		return 0;
	}

	private void init()
	{
		if (bufferedInputStream == null)
		{
			try
			{
				bufferedInputStream = new BufferedReader(new InputStreamReader(new BufferedInputStream(new FileInputStream(pipeFile))));
			}
			catch (IOException e)
			{
				Throwables.propagate(e);
			}
		}
	}

	@Override
	public void close()
	{
		try
		{
			bufferedInputStream.close();
		}
		catch (IOException e)
		{}
	}
}
